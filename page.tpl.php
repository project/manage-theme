<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<?php
echo $head;
echo $styles;
?>
<!--[if IE]>
<style type="text/css">
body {behavior: url(<?php echo path_to_theme() . '/csshover.htc'; ?>);}
</style>
<noscript>
<style type="text/css">
.nav .dropdown, .nav .dropdown div {width: 189px;}
.nav .button .dropdown ul {margin: 0px;}
.nav .dropdown, .nav .dropdown div {position: static;}
.nav .dropdown ul {border: 0;}
.mini-zone {display: none;}
</style>
</noscript>
<![endif]-->
<!-- The above block calls the special .htc script that forces compliance in IE/win,
and also "dumbs down" the nav when IE is set not to allow scripting. Only IEwin 
can read this block. -->
</head>
<body><div id="wrapper">
<?php echo manage_topmenu(); ?>
<div class="textbox">
<!--div class="back">
<a href="/logout"><strong>Abmelden</strong></a>
</div-->
<?php echo $breadcrumb; ?>
<h1 class="title"><?php print $title; ?></h1>
<div class="tabs"><?php print $tabs; ?></div>
<?php print $help; ?>
<?php print $messages; ?>
<?php print $content; ?>
</div></body>
</html>
