<?php
/**
 *
 */

function manage_topmenu($mid=0, $level=0) {
  $menu = menu_get_menu();

  if (!$mid) {
    $mid = intval(variable_get('missing_menu', '1'));
  }
  $topmenu = $menu['visible'][$mid]['children'];
  if (count($topmenu) == 0) {
    return "\n";
  }

  $seq = array('one', 'two', 'three', 'four', 'five');
  $prefix = str_repeat('   ', $level);

  $output = "\n" . $prefix;
  switch ($level) {
    case 0:
      $output .= '<div class="nav">';
      break;
    case 1:
      $output .= '<div class="dropdown">';
      break;
    default:
      $output .= '<div>';
      break;
  }
  $output .= "\n" . $prefix . " <ul>\n";

  $i = 0;

  $cnt = count($topmenu);

  foreach ($topmenu as $mid) {
    $submenu = $menu['visible'][$mid];

    switch ($level) {
      case 0:
        $output .= $prefix . sprintf('  <li class="button"><div class="parent %s">%s',
          $seq[$i], l($submenu['title'], $submenu['path'])
        );
        $output .= manage_topmenu($mid, $level+1);
        $output .= $prefix . "  </div></li>\n";
        break;

      case 1:
        $output .= $prefix . sprintf('  <li style="z-index: %d;">%s<div class="mini-zone"></div>',
          $cnt - $i, l($submenu['title'], $submenu['path']));
        $output .= manage_topmenu($mid, $level+1);
        $output .= $prefix . "  </li>\n";
        break;
 
      default:
        $output .= $prefix . '  <li>' . l($submenu['title'], $submenu['path']);
        $output .= manage_topmenu($mid, $level+1);
        $output .= $prefix . "  </li>\n";
        break;
    }
    $i++;
  }
  $output .= $prefix . ' </ul>';
  if ($level == 0) {
    $output .= '<br class="brclear" />';
  }
  $output .= "\n" . $prefix . "</div>\n";
  return $output;
}

function manage_style() {
    return _phptemplate_callback('style');
}
